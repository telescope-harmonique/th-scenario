#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Flat2Tree"""

import os

import csv
import json

IMAGES_PATH = "../web/images"
IMAGES_PATH_FS = os.path.join(IMAGES_PATH, "fullsize")


def row2elem(header, row):
    new_elem = {}
    new_elem["id"] = row[2]
    new_elem["depth"] = timecode2depth(new_elem["id"])
    new_elem["splitted"] = False
    data = {}
    for i in range(0, len(header)):
        data[header[i]] = row[i]
    new_elem["data"] = data
    new_elem["children"] = []
    return new_elem


def timecode2depth(timecode):
    return len(timecode.split("."))


def get_parent_timecode(timecode):
    if len(timecode.split(".")) == 1:
        return "00"
    return ".".join(timecode.split(".")[:-1])


def get_slideshow(directory):
    lst_images = os.listdir(directory)

    loc = os.path.split(directory)[-1]

    lst_images = [os.path.join(loc, i) for i in lst_images if i.split(".")[-1].lower() in ["png", "jpeg", "jpg", "gif", "svg"]]
    lst_images.sort()
    print(lst_images)

    return lst_images


def get_all_images():
    all_images = {}
    all_dirs = os.listdir(IMAGES_PATH_FS)
    for parcours in all_dirs:
        path_dir = os.path.join(IMAGES_PATH_FS, parcours)
        all_images[parcours] = {}
        for image in os.listdir(path_dir):
            ext = image.split(".")
            if len(ext) == 2:
                if ext[1].lower() in ["jpg", "jpeg", "gif", "png", "svg"]:
                    all_images[parcours][ext[0].replace("-", ".")] = [image]
            elif len(ext) == 1:
                slideshow_path = os.path.join(path_dir, image)
                if os.path.isdir(slideshow_path):
                    print("is dir")
                    all_images[parcours][ext[0].replace("-", ".")] = get_slideshow(slideshow_path)

    return all_images


class Flat2tree:
    def __init__(self, csv_path):
        print("ok")
        self.csv_filename = csv_path
        self.root = {"id": "00",
                     "depth": 0,
                     "children": [],
                     "split": False,
                     "data": None}
        self.elems = []
        self.elems_by_id = {"00": self.root}

        self.process()

    def read_csv(self):
        with open(self.csv_filename) as csv_file:
            tab = []
            reader = csv.reader(csv_file, delimiter=",", quotechar='"')
            for row in reader:
                tab.append(row)
            header = tab.pop(0)
            for line in tab:
                new_elem = row2elem(header, line)
                self.elems.append(new_elem)
                self.elems_by_id[new_elem["id"]] = new_elem


    def get_depth(self, depth):
        lst = [elem for elem in self.elems if elem["depth"] == depth and elem["id"] != "00"]

        for elem in lst:
            # print(self.root)
            parent_timecode = get_parent_timecode(elem["id"])

            if parent_timecode in self.elems_by_id.keys():
                parent = self.elems_by_id[parent_timecode]
                parent["children"].append(elem)
            elif elem["id"][1:] in self.elems_by_id.keys():
                self.elems_by_id[elem["id"][1:]]["children"].append(elem)
            else:
                new_id = elem["id"][1:]
                new_parent = {"id": new_id,
                              "children": [elem],
                              "split": True,
                              "data": None,
                              "depth": depth}

                self.elems_by_id[get_parent_timecode(new_id)]["children"].append(new_parent)
                self.elems_by_id[new_id] = new_parent

    def create_tree(self):
        self.get_depth(1)
        self.get_depth(2)
        self.get_depth(3)

    def add_images(self):
        imgs = get_all_images()
        for elem in self.elems:
            if elem["data"]:
                elem["data"]["images"] = {}
                for parcours in elem["data"]:
                    if parcours in imgs:
                        if elem["id"] in imgs[parcours]:
                            elem["data"]["images"][parcours] = imgs[parcours][elem["id"]]
                            
    def process(self):
        self.read_csv()
        self.create_tree()
        self.add_images()

    def export(self, filename, data=True):
        if not data:
            for elem in self.elems:
                elem.pop("data")

        with open(filename, "w") as f:
            json.dump(self.root, f, indent=2)
