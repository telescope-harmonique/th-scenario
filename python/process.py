#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Process Flat2Tree"""

from flat2tree.flat2tree import Flat2tree, get_all_images


f = Flat2tree("data.csv")
f.export("out.json")
f.export("out_nodata.json", data=False)

images = get_all_images()
print(images)
