
const elems = [];
let cur_depth = 3;
let mode = "story";
let show_infos = true;

let tree_control_visible = true;

let btn_control_mode_story, btn_control_mode_tree;
let btn_infos;
let panel_control_tree, panel_control_story;

let story_svg;
let svg_width = 280;
let svg_height = 1000;

let verbose = false;

let story_mod = "A";
let story_current = undefined;

const elems_by_id = {};

const meta_cat = [];

const meta_data = [];
////////////////////////////////////
// LAYOUT                         //
////////////////////////////////////

const panel_container = d3.select("body").append("div")
      .attr("class", "panel");

const panel_header = panel_container.append("div").attr("class", "panel-header");

const panel_infos = panel_container.append("div").attr("class", "panel-content panel-infos");
panel_infos.classed("hide", !show_infos);
panel_infos.append("h2").text("Bienvenue sur Th-Scénario");
panel_infos.append("p").text("Cet outil rassemble tous les éléments du scénario du Télescope Harmonique.");
panel_infos.append("p").text("Ces éléments sont affichés dans une des deux vues que l'on peut activer avec les boutons en haut de ce panneau : \"Storyboard\" et \"script\".");
panel_infos.append("h3").text("Vue Storyboard");
panel_infos.append("p").text("Cette vue affiche le déroulement sous la forme d'un diaporama.");
panel_infos.append("p").text("On peut naviger entre les différentes étapes avec le clavier.");
panel_infos.append("h3").text("Vue Script");
panel_infos.append("p").text("Cette vue affiche toutes les étapes sous forme d'un tableau.");
panel_infos.append("p").text("Dans cette vue, le panneau de droite affiche soit :");
const liste = panel_infos.append("ul");
liste.append("li").html("<b>Lecture</b> : Le contenu de l'étape survolée avec la souris.");
liste.append("li").html("<b>Réglages</b> : Des contrôles permettant de choisir les éléments affichés dans la vue.");


const panel_tree = panel_container.append("div").attr("class", "hide panel-content panel-tree");
const panel_tree_header = panel_tree.append("div").attr("class", "panel-header");
const panel_tree_content = panel_tree.append("div").attr("class", "panel-tree-content");
const panel_tree_control = panel_tree.append("div").attr("class", "panel-tree-control");

const panel_story = panel_container.append("div").attr("class", "hide panel-content panel-story");


const btn_tree_view = panel_tree_header.append("a").attr("class", "th-btn th-btn-tree th-btn-control");
btn_tree_view.append("i").attr("class", "fi-xwsrxl-caret-solid");
btn_tree_view.append("span").text("Lecture");
btn_tree_view.on("click", () => toggle_control(false));

const btn_tree_control = panel_tree_header.append("a").attr("class", "th-btn th-btn-tree th-btn-control active");
btn_tree_control.append("i").attr("class", "fi-xnsuxs-tools-solid");
btn_tree_control.append("span").text("Réglages vue");
btn_tree_control.on("click", () => toggle_control(true));

const tree_container = d3.select("body").append("div")
      .attr("class", "tree-container");
const story_container = d3.select("body").append("div").attr("class", "story-container");

function set_mode(mode_in){
    mode = mode_in;
    show_infos = false;
    d3.select(".th-btn-infos").classed("active", false);
    panel_infos.classed("hide", true);
    btn_control_mode_story.classed("active", mode == "story");
    btn_control_mode_tree.classed("active", mode == "tree");
    tree_container.classed("hide", mode != "tree");
    story_container.classed("hide", mode != "story");
    panel_tree.classed("hide", mode != "tree");
    panel_story.classed("hide", mode != "story");
}


function show_panel(elem){
    if (tree_control_visible) return;
    panel_tree_content.classed("hide", false);
    panel_tree_control.classed("hide", true);
    panel_tree_content.html("");

    panel_tree_content.append("h3").attr("class", "timecode").html(elem.id);

    if (elem.data) {
        panel_tree_content.append("h2").html(elem.data.titre);
        for (const cat of meta_cat){
            for (const param of cat.children){
        // for (const [key, value] of Object.entries(elem.data)){
                let key = param.cle;
                let value = elem.data[key];

                let do_create = key in elem.data.images;
                do_create = do_create || value && value.trim != "";

                if (do_create) {
                    const data_title = panel_tree_content.append("h3");

                    data_title.append("i").attr("class", param.icone);
                    data_title.append("span")
                    .html(param.titre);

                    if (key in elem.data.images){
                        if (elem.data.images[key].length == 1){
                            panel_tree_content.append("img")
                                .attr("src", "images/fullsize/" + key + "/" + elem.data.images[key]);
                        }else{
                            panel_tree_content.append("img")
                                .attr("src", "images/fullsize/" + key + "/" + elem.data.images[key][0]);
                            console.log("multiple images");
                        }
                    }

                    panel_tree_content.append("p")
                        .html(value);
                }
            }
        }
    }

    friconix_update();
}


////////////////////////////////////
// CONTROL PANEL                  //
////////////////////////////////////

function toggle_infos(visible=undefined){
    if (visible != undefined){
        show_infos = visible;
    }else{
        show_infos = !show_infos;
    }

    if (!show_infos){
        btn_infos.classed('active', false);
        set_mode(mode);
    }else{
        btn_infos.classed('active', true);
        d3.selectAll(".panel-content").classed("hide", true);
    }
    panel_infos.classed("hide", !show_infos);
}

function create_panel(){
    //----------------------------------------------------//
    // create control mode


    const mode_control = panel_header.append("span").attr("class", "mode-control");
    btn_control_mode_story = mode_control.append("a").attr("class", "th-btn th-btn-mode").text("Storyboard");
    btn_control_mode_tree = mode_control.append("a").attr("class", "th-btn th-btn-mode").text("Script");

    btn_control_mode_tree.on("click", () => set_mode("tree"));
    btn_control_mode_story.on("click", () => set_mode("story"));

    btn_infos = panel_header.append("a").attr("class", "th-btn th-btn-infos");
    btn_infos.append("i").attr("class","fi-cnsuxl-info-solid");

    btn_infos.on("click", () => toggle_infos() );
}

function toggle_control(visible){
    tree_control_visible = visible;
    btn_tree_control.classed("active", tree_control_visible);
    btn_tree_view.classed("active", !tree_control_visible);
    panel_tree_content.classed("hide", tree_control_visible);
    panel_tree_control.classed("hide", !tree_control_visible);

    friconix_update();
}

function create_control(){
    //----------------------------------------------------//
    // create control script/tree mode

    panel_tree_control.append("h3").text("Afficher ou cacher des parcours");

    let elem = panel_tree_control.append("div").attr("class", "th-control-elem");

    elem.append("span").text("Tous les parcours");

    // let elem_btn_hl = elem.append("a").attr("class", "th-btn th-btn-hl");
    // let icon_hl = elem_btn_hl.append("i").attr("class", "fi-xnsuxl-highlighter-solid");

    let elem_btn = elem.append("a").attr("class", "th-btn th-btn-show");
    let icon = elem_btn.append("i").attr("class", "fi-xnluxl-eye");


    let show_all_data = true;
    elem_btn.on("click", ()=>{
        if (show_all_data){
            show_all_data = false;
            d3.selectAll(".data-elem").classed("hide", true);
            d3.selectAll(".th-btn-show i").attr("class", "fi-xnpuxl-eye");
            for (const value of meta_data){
                value.hidden = true;
            }
        }else{
            show_all_data = true;
            d3.selectAll(".data-elem").classed("hide", false);
            d3.selectAll("a.th-btn-show i").attr("class", "fi-xnluxl-eye");
            for (const value of meta_data){
                value.hidden = false;
            }
        }
        friconix_update();
    });

    for (const value of meta_data){
        let key = value.cle;
        let elem = panel_tree_control.append("div").attr("class", "th-control-elem");
        elem.append("i").attr("class", "th-btn-icon " + value.icone);
        elem.append("span").text(value.titre);

        // let elem_btn_hl = elem.append("a").attr("class", "th-btn th-btn-hl");
        // let icon_hl = elem_btn_hl.append("i").attr("class", "fi-xnsuxl-highlighter-solid");

        let elem_btn = elem.append("a").attr("class", "th-btn th-btn-show");
        let icon = elem_btn.append("i").attr("class", "fi-xnluxl-eye");
        elem_btn.on("click", ()=> {
            if (value.hidden){
                value.hidden = false;
                icon.attr("class", "fi-xnluxl-eye");
            }else{
                value.hidden = true;
                icon.attr("class", "fi-xnpuxl-eye");
            }
            console.log("click", "data-"+key);

            d3.selectAll(".data-"+key).classed("hide", value.hidden);
            friconix_update();
        });

    }

    panel_tree_control.append("h3").text("Niveaux visibles");
    let control_depth = panel_tree_control.append("div").attr("class", "th-control-elem");
    control_depth.append("span").text("Cacher les niveaux supérieurs à");

    let control_select = control_depth.append("select").attr("id", "depth-select");
    control_select.append("option").attr("value", "3").text("3");
    control_select.append("option").attr("value", "2").text("2");
    control_select.append("option").attr("value", "1").text("1");

    control_select.on("change", function(e) {
        cur_depth = parseInt(d3.select(this).node().value);
        console.log(cur_depth);
        for (let i = 0; i<= cur_depth; i++){
            d3.selectAll(".th-node-"+i).transition().duration(750)
                .style("max-height","10000px")
                .style("margin", "unset")
                // .style("padding","5px")
                .style("border-color", "rgba(0,0,0,1)");
        }

        for (let i = cur_depth+1; i< 5; i++){
            d3.selectAll(".th-node-"+i).transition().duration(750)
                .style("max-height", "0px")
                .style("margin", "none")
                // .style("padding", "0px")
                .style("border-color", "rgba(0,0,0,0)");
        }
    });

    //----------------------------------------------------//
    // create control storyboard

    let select_story = panel_story.append("h3").text("Choix de l'équipe");

    let control_select_story = select_story.append("select").attr("id", "th-story-select");
    control_select_story.append("option").attr("value", "A").text("A");
    control_select_story.append("option").attr("value", "B").text("B");
    control_select_story.append("option").attr("value", "C").text("C");

    control_select_story.on("change", function(e) {
        story_mod = d3.select(this).node().value;
        update_graph();
    });

    panel_story.append("h3").text("Position");
    story_svg = panel_story.append("svg")
        .attr("width", svg_width + "px")
        .attr("height", svg_height + "px");

    create_graph(story_svg);
    friconix_update();
}

function get_next_mod(elem, mod){
    console.log("next", elem.next, mod);
    for (const next_elem of elem.next){
        if ("ABC".indexOf(get_module(next_elem)) == mod){
            return next_elem;
        }
    }
    // return elem.next[0];
    return undefined;
}

let paths = [];
function get_graph_pos(mod){
    let cpt = 2;
    let cur_elem = elems[0];
    cur_elem.pos_y = 1;
    cur_elem.x = svg_width/2;

    let x = svg_width/3 + mod*svg_width/6;

    paths.push([cur_elem]);
    while (cur_elem.next.length > 0){
        if (cur_elem.next.length == 1){
            cur_elem = cur_elem.next[0];
            if (get_module(cur_elem)){
                cur_elem.x = x;
            }else{
                cur_elem.x = svg_width/2;
            }
        }else{
            cur_elem = get_next_mod(cur_elem, mod);
            cur_elem.x = x;
        }

        if (cur_elem.pos_y){
            cur_elem.pos_y = Math.max(cur_elem.pos_y, cpt);
        }else{
            cur_elem.pos_y = cpt;
        }
        paths[mod].push(cur_elem);
        cpt += 1;
    }
}

const lines = [];
function get_graph_pos_bis(){
    let step = 27;

    for (const elem of elems){
        if (!elem.split) {
            if (!get_module(elem)){
                elem.x = svg_width/2;
            }else{
                elem.x = svg_width/3 + get_module_mod(elem)*svg_width/6;
            }
        }

    }
    elems[0].y = step;
    for (const elem of elems){
        let is_module = false;
        for (const next_elem of elem.next){
            next_elem.y = elem.y + step;
            is_module = get_module(elem) || get_module(next_elem);
            if (!is_module) is_module = "all";

            elem.mod = is_module;

            lines.push({source: elem,
                        target: next_elem,
                        path: is_module});
        }
    }
}

function create_graph(svg){
    get_graph_pos_bis();
    for (const line of lines){
        svg.append("path")
            .attr("class", () => "th-line-"+line.path)
            .style("stroke", () => {
                if (line.path == "all" || line.path == story_mod){
                    return "white";
                }else{
                    return "black";
                }
            })
            .attr("d", d3.line()([[line.source.x, line.source.y],[line.target.x, line.target.y]]));
    }

    for (const elem of elems){
        if (!elem.split){
            let new_circle = svg.append("circle")
                .style("fill", () => {
                    if (!get_module(elem) || get_module(elem) == story_mod){
                        return "white";
                    }else{
                        return "black";
                    }
                })
                .attr("class", () => {
                    let mod = get_module(elem);
                    if (!mod) mod = "all";
                    return "th-circle-"+mod;
                })
                .attr("id", () => "circle-"+elem.id.replaceAll(".", "-"))
                .attr("cx", ()=> elem.x)
                .attr("cy", () => elem.y)
                .attr("r", ()=> 15 - 3 * elem.depth);

            new_circle.on("mouseover", ()=>{
                console.log(elem.id);
            });

            new_circle.on("click", ()=>create_story(elem));
        }
    }
}

function update_graph(){
    story_svg.selectAll("path")
        .style("stroke", function() {
            return d3.select(this).classed("th-line-all") ? "white" : "black";
        });

    story_svg.selectAll(".th-line-" + story_mod)
        .style("stroke", "white");

    story_svg.selectAll("circle")
        .style("fill", function() {
            return d3.select(this).classed("th-circle-all") ? "white" : "black";
        });

    story_svg.selectAll(".th-circle-" + story_mod)
        .style("fill", "white");
}

////////////////////////////////////
// SCRIPT/TREE VIEW               //
////////////////////////////////////

function create_title(node, elem){
    let title = node.append("div");

    title.append("span")
        .attr("class", "title depth-" + elem.depth)
        .text(elem.id + " " + elem.data.titre);

    if (elem.children.length>0){
        let plus = title.append("span")
            .attr("class", "th-btn-expand");

        plus.append("i")
            .attr("class", "fi-xnluxm-eye");

        plus.on("click", function(){
            if (elem.hide == 0){
                node.selectAll(".children").classed("hidden", true);
                elem.hide = 1;
            }else if (elem.hide == 1){
                node.select(".children").classed("hidden", false);
                elem.hide = 2;
            }else if (elem.hide == 2){
                node.selectAll(".children").classed("hidden", false);
                elem.hide = 0;
            }
        });
    }
}

function create_data(node, elem){
    let data_node = node.append("div").attr("class", "th-data");
    if (elem.data) {
        if (elem.data.note_todo && elem.data.note_todo != ""){
            console.log("todo");
            node.classed("th-todo", true);
        }
        for (const data of meta_data){
            const key = data.cle;
            const value = elem.data[key];

            if (value && value.trim() != "") {
                const data_elem = data_node.append("div")
                      .attr("class", "data-elem data-"+key);

                data_elem.on("mouseover", ()=> {
                    d3.selectAll(".data-elem").classed("highlight", false);
                    d3.selectAll(".data-" + key).classed("highlight", true);
                });
                data_elem.on("mouseleave", ()=> {
                    d3.selectAll(".data-elem").classed("highlight", false);
                });

                data_elem.append("i")
                    .attr("class", data.icone);

                data_elem.append("div")
                    .attr("class", "data-content")
                    .html(value);
            }
        }
    }

}


////////////////////////////////////
// STORYBOARD VIEW                //
////////////////////////////////////

let cur_num_images = 0;
let cur_images = [];
let cur_images_key = undefined;
let end_images = true;

function next_image(){
    if (cur_num_images == cur_images.length){
        return false;
    }else if (cur_num_images == cur_images.length - 1){
        return false;
    }
    console.log(cur_num_images, cur_images.length);
    cur_num_images = cur_num_images + 1;
    d3.select(".cur-slideshow")
        .attr("src", "images/fullsize/" + cur_images_key + "/" + cur_images[cur_num_images]);
    return true;
}

function get_next_story(){
    let story_next = false;
    if (story_current.next.length == 1){
        story_next = story_current.next[0];
    }else{
        for (const next_elem of story_current.next){
            if (get_module(next_elem) == story_mod){
                story_next = next_elem;
            }
        }
    }
    if (story_next){
        return story_next;
    }else{
        return story_current;
    }
}

function next_story(){
    if (next_image()){
        return;
    }
    story_current = get_next_story();
    create_story(story_current);
}

function prev_image(){
    console.log(cur_num_images, cur_images.length);
    cur_num_images = cur_num_images - 1;
    d3.select(".cur-slideshow")
        .attr("src", "images/fullsize/" + cur_images_key + "/" + cur_images[cur_num_images]);
}

function get_prev_story(){
    let story_prev = false;
    if (story_current.prev.length == 1){
        story_prev = story_current.prev[0];
    }else{
        for (const prev_elem of story_current.prev){
            if (get_module(prev_elem) == story_mod){
                story_prev = prev_elem;
            }
        }
    }
    if (story_prev){
        return story_prev;
    }else{
        return story_current;
    }
}

function prev_story(){
    if (cur_images_key && cur_num_images > 0){
        prev_image();
        return;
    }
    story_current = get_prev_story();
    create_story(story_current);
}

function next_module(){
    let cur_num = "ABC".indexOf(story_mod);
    cur_num = (cur_num + 1) % 3;
    story_mod = "ABC"[cur_num];
    d3.select("#th-story-select").node().value = story_mod;
    find_next_module();
    update_graph();
}

function prev_module(){
    let cur_num = "ABC".indexOf(story_mod);
    cur_num = (cur_num - 1);
    if (cur_num < 0) cur_num = 2;
    story_mod = "ABC"[cur_num];
    d3.select("#th-story-select").node().value = story_mod;
    find_next_module();
    update_graph();
}

function find_prev_no_module(){
    let current_prev = story_current.prev[0];
    while (current_prev.data.module != ""){
        current_prev = current_prev.prev[0];
    }
    return current_prev;
}

function find_next_module(){
    let cur_module = story_current.data.module;
    let cur_etape = story_current.data.etape;
    if (cur_module != ""){
        let next_tc = elems_by_id[story_mod+cur_etape];
        if (next_tc){
            story_current = next_tc;
        }else{
            story_current = find_prev_no_module();
        }
        create_story(story_current);
    }
}

document.addEventListener('keydown', (event) => {
    const key_name = event.key;
    if (key_name == "ArrowDown"){
        next_story();
    }else if(key_name == "ArrowUp"){
        prev_story();
    }else if(key_name == "ArrowRight"){
        next_module();
    }else if(key_name == "ArrowLeft"){
        prev_module();
    }
    console.log(key_name);
});


function create_story(elem){
    cur_num_images = 0;
    cur_images_key = undefined;
    cur_images = [];

    story_svg.selectAll("circle").classed("circle-hl", false);
    story_svg.select("#circle-"+elem.id.replaceAll(".", "-")).classed("circle-hl", true);

    story_container.html("");
    const story_nav = story_container.append("div").attr("class", "th-story-nav");

    const story_title = story_nav.append("h1");
    story_title.append("span").attr("class", "th-story-title-timecode").text(elem.id);
    story_title.append("span").text(elem.data.titre);

    const story_next = get_next_story();
    if (story_next != story_current){
        const story_after = story_nav.append("a").attr("class", "th-btn th-story-nav-after");
        story_after.append("i").attr("class", "fi-hwsdxl-chevron-solid");
        story_after.append("span").attr("class", "th-btn-story-nav").text(story_next.id);
        story_after.on("click", () => next_story());
    }

    const story_prev = get_prev_story();
    if (story_prev != story_current){
        const story_before = story_nav.append("a").attr("class", "th-btn th-story-nav-before");
        story_before.append("span")
              .attr("class", "th-btn-story-nav")
              .text(story_prev.id);

        story_before.on("click", () => prev_story());
        story_before.append("i").attr("class", "fi-hwsuxl-chevron-solid");
    }




    const story_data_container = story_container.append("div").attr("class", "th-story-data-container");
    create_data_story(story_data_container, elem);

    friconix_update();
}

function create_data_story(container, elem){
    for (const value of meta_cat){
        let story_data = container.append("div").attr("class", "th-story-data th-story-data-" + value.key);

        story_data.append("h2").text(value.titre);

        let story_data_container = story_data.append("div").attr("class", "th-story-data-parcours-container");

        for (const data_children of value.children ){
            create_data_story_parcours(story_data_container, elem, data_children);
        }

    }
}

function create_data_story_parcours(container, elem, parcours){
    let story_data_parcours = container.append("div").attr("class", "th-story-data-parcours th-story-data-parcours-" + parcours.cle);
    const data_title = story_data_parcours.append("h3");

    const key = parcours.cle;
    data_title.append("i").attr("class", parcours.icone);
    data_title.append("span")
        .html(parcours.titre);

    if (key in elem.data.images){
        if (elem.data.images[key].length == 1){
            story_data_parcours.append("img")
                .attr("src", "images/fullsize/" + parcours.cle + "/" + elem.data.images[parcours.cle][0]);
        }else if (elem.data.images[key].length >= 1){
            cur_images_key = key;
            cur_images = elem.data.images[key];
            story_data_parcours.append("img")
                .attr("src", "images/fullsize/" + parcours.cle + "/" + elem.data.images[parcours.cle][0])
                .attr("class", "cur-slideshow");
        }
    }else{
        if (parcours.cle == "interface_module"){
            story_data_parcours.append("img").attr("src", "images/blank.png");
        }
    }

    if (elem.data[parcours.cle]){
        story_data_parcours.append("p").text(elem.data[parcours.cle]);
    }else{
        story_data_parcours.append("p").text(find_last_data(elem, parcours.cle));
    }
}

function find_last_data(elem, key){
    let cur_elem = elem;
    if (cur_elem.depth == 1) return "";
    while (cur_elem.data[key] == ""){
        cur_elem = cur_elem.prev[0];
        if (cur_elem.depth == 1){
            return "";
        }
    }
    return cur_elem.data[key];
}

////////////////////////////////////
// INIT TREE                      //
////////////////////////////////////

function create_children(node, parent, splitted){
    node.children.forEach(e => {
        elems.push(e);
        e.parent = node;
        e.hide = 0;
        e.hl = false;
        elems_by_id[e.id] = e;
        let child = parent.select(".children")
            .append("div")
            .attr("class", "th-node th-node-"+e.depth);

        e.node = child;

        child.on("mouseover", (a)=> {
            d3.selectAll(".th-node").classed("highlight", false);
            show_panel(e);
            child.classed("highlight", true);
            a.stopPropagation();
        });

        if (e.split){
            child.classed("split", true);
        }else{
            create_title(child, e);
        }

        create_data(child, e);

        child.append("div")
            .attr("class", "children");
        if (e.children.length>0){
            create_children(e, child);
        }
    });
};

//----------------------------------------------------//
// create topo utils

function get_sister_after(elem){
    const pos = elem.parent.children.indexOf(elem);
    if (pos < elem.parent.children.length){
        return elem.parent.children[pos+1];
    }
    return undefined;
}

function get_module(elem){
    // console.log(elem);
    let pos = "ABC".indexOf(elem.id.slice(0, 1));
    if (pos >= 0){
        return "ABC"[pos];
    }else{
        return undefined;
    }
}

function get_paths(){
    for (const elem of elems){
        elem.next = get_story_after(elem);
    }

    for (const elem of elems){
        elem.prev = get_story_before(elem);
    }
}
function get_module_mod(elem){
    return "ABC".indexOf(get_module(elem));
}

function is_mod_in(elems, mod){
    for (const elem of elems){
        if (get_module_mod(elem) == mod){
            return true;
        }
    }
    return false;
}

function get_mod_index(elems, mod){
    for (let i = 0; i<elems.length; i++){
        let elem = elems[i];
        if (get_module_mod(elem) == mod){
            return i;
        }
    }
    return false;
}

//----------------------------------------------------//
// create topo main

function find_next(cur_elem, mod){
    const elem_prev = cur_elem;
    let ok = false;
    let v = false;

    let cur_parent = cur_elem.parent;

    while(!ok){
        let sister_after = get_sister_after(cur_parent);
        if (sister_after){
            if (v) console.log("parent has sister after");
            if (get_module_mod(sister_after) == mod){
                if (v) console.log("same module");
                return sister_after;
            }else if (!get_module(sister_after)){
                if (v) console.log("not module");
                return sister_after;
            }else{
                if (v) console.log("sister parent not same module");
                cur_parent = cur_parent.parent;
            }
        }else{
            if (v) console.log("parent has not sister");
            cur_parent = cur_parent.parent;
        }
        if (cur_parent.id == "00"){
            if (v) console.log("get back to root");
            ok = true;
        }
    }

    if (v) console.log("cannot find next");
    return undefined;
}

function get_path_module(mod){
    let cur_elem = elems[0];
    let elems_seq = [];
    let v = false;

    while (cur_elem){
        if (v) console.log("FIND AFTER", cur_elem.id);
        if (!cur_elem.split){
            elems_seq.push(cur_elem);
        }
        if (cur_elem.children.length>0){
            if (v) console.log("has children");
            if (get_module(cur_elem)){
                if (v) console.log("[ok] is module first child");
                cur_elem = cur_elem.children[0];
            }else if (cur_elem.split){
                if (v) console.log("is splitted");
                if (is_mod_in(cur_elem.children, mod)) {
                    if (v) console.log("[ok] is split with mod");
                    cur_elem = cur_elem.children[get_mod_index(cur_elem.children, mod)];
                }else{
                    if (v) console.log("is split without mod");
                    let sister_after = get_sister_after(cur_elem);
                    if (sister_after){
                        if (v) console.log("ok has sister");
                        cur_elem = sister_after;
                    }else{
                        cur_elem = find_next(cur_elem, mod);
                    }
                }
            }else{
                if (v) console.log("[ok] is not splitted");
                cur_elem = cur_elem.children[0];
            }
        }else{
            if (v) console.log("has not children");
            if (get_module(cur_elem)){
                if (v) console.log("is module");
                let sister_after = get_sister_after(cur_elem);
                if (sister_after){
                    if (v) console.log("has sister after");
                    if (get_module_mod(sister_after) == mod){
                        if (v) console.log("[ok] is same mod");
                        cur_elem = sister_after;
                    }else{
                        if (v) console.log("is not same mod");
                        cur_elem = find_next(cur_elem, mod);
                    }
                }else{
                    if (v) console.log("has not sister after");
                    cur_elem = find_next(cur_elem, mod);
                }
            }else{
                if (v) console.log("is not module");
                let sister_after = get_sister_after(cur_elem);
                if (sister_after){
                    if (v) console.log("[ok] has sister after");
                    cur_elem = sister_after;
                }else{
                    if (v) console.log("has not sister after");
                    cur_elem = find_next(cur_elem, mod);
                }
            }
        }
    }

    if (v) console.log(elems_seq);

    for (const elem of elems_seq){
        if (v) console.log(elem.id);
    }
    return elems_seq;
}

function create_topo(){
    const paths = {};
    for (const elem of elems){
        elem.next = [];
        elem.prev = [];
    }

    for (let i = 0; i<3;i++){
        let path = get_path_module(i);
        for (let i=0; i<path.length-1;i++){
            let elem = path[i];
            let elem_plus = path[i+1];

            if (elem.next.indexOf(elem_plus)<0) elem.next.push(elem_plus);
            if (elem_plus.prev.indexOf(elem)<0) elem_plus.prev.push(elem);
        }
    }

}


function create_meta(meta_data_in){
    let meta_dic = {};

    meta_data_in.forEach((elem) => {
        elem.hidden = false;
        if (!elem.parent){
            const new_cat = {key: elem.cle,
                             titre: elem.titre,
                             children: [],
                            };
            meta_dic[elem.cle] = new_cat;
            meta_cat.push(new_cat);
        }else{
            meta_data.push(elem);
            meta_dic[elem.parent].children.push(elem);
        }
    });
}

d3.csv("data_meta.csv")
    .then(function(meta_data_in){
        create_meta(meta_data_in);

        d3.json("data.json")
            .then(function(data){
                console.log(data);

                let root = tree_container.append("div");

                root.append("div")
                    .attr("class", "children");
                create_children(data, root, false);

                create_topo();

                create_panel();
                create_control();

                story_current = elems[0];
                create_story(elems[0]);
                set_mode("story");

                toggle_infos(true);

                friconix_update();
            });
    });

